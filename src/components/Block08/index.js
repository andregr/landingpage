import React from 'react'
import styled from 'styled-components'

import { TitleDiv, Title, SizedTitle } from '../StyledComponents'

import bgImage from '../../images/bg-pontinhos-8.png'
import stepsImage from '../../images/passo-a-passo-8.png'
import trophieImage from '../../images/trofeu-8.png'

const Bg = styled.div`
  width: 100%;
  background-color: #251938;
  background-image: url(${bgImage});
  position: relative;
`

const Image = styled.img`
  max-width: 1394px;
  padding: 0;

  @media screen and (max-wdith: 468px) {
    margin: 30px auto;
  }
`

const Subtitle = styled.h2`
  font-size: 18px;
  text-shadow: 1px 1px #000;
  font-weight: 600;
`

const Grid = styled.div`
  color: white;
  font-size: 12px;
  display: grid;
  grid-template-columns: auto auto auto auto auto;
  grid-template-rows: auto auto auto auto auto auto;
  justify-content: space-evenly;
  align-content: center;

  @media screen and (max-width: 625px) {
    grid-template-columns: auto;
    grid-template-rows: auto auto auto auto auto auto auto;
  }
`

const StepsImage = styled.div`
  grid-column: 1 / span 4;
  grid-row: 2;
  
  @media screen and (max-width: 1600px) {
    padding-right: 20px;
  }

  @media screen and (max-width: 1550px) {
    padding-right: 30px;
  }

  @media screen and (max-width: 1500px) {
    padding-right: 40px;
  }


  @media screen and (max-width: 1023px) {
    padding-right: 80px;
  }

  @media screen and (max-width: 625px) {
    grid-column: 1;
    grid-row: 1;
    padding-right: 0px;
  }
`

const TrophieImage = styled.div`  
  position: absolute;
  padding: 10px 0px 10px 0px;
  right: 0;
  top: 230px;
  
  @media screen and (max-width: 1600px) {
    width: 200px;
  }

  @media screen and (max-width: 1550px) {
    width: 190px;
  }

  @media screen and (max-width: 1500px) {
    width: 180px;
  }

  @media screen and (max-width: 1337px) {
    width: 170px;
  }

  @media screen and (max-width: 1300px) {
    width: 130px;
    top: 250px;
  }

  @media screen and (max-width: 1152px) {
    width: 100px;
    top: 270px;
  }

  @media screen and (max-width: 1023px) {
    width: 90px;
  }

  @media screen and (max-width: 890px) {
    width: 70px;
    top: 290px;
  }


  @media screen and (max-width: 625px) {
    position: relative;
    height: 100px;
    width: 150px;
    margin: 0px auto 0px auto;
    padding-bottom: 90px;
    top: -120px;
  }
`

const ImediateAccess = styled.div`
  grid-column: 1;
  grid-row: 3;
  position: relative;
  top: -60px;
  max-width: 250px; 
  padding-top: 5px;

  @media screen and (max-width: 1215px) {
    top: -50px;
  }

  @media screen and (max-width: 1024px) {
    max-width: 200px;
  }

  @media screen and (max-width: 940px) {
    top: -45px;
  }

  @media screen and (max-width: 625px) {
    grid-column: 1;
    grid-row: 2;
    max-width: 350px;
    position: static;
    padding: 10px;
    margin: 0 auto;
  }
`

const StepByStepDiv = styled.div`
  grid-column: 2;
  grid-row: 1;
  position: relative;
  top: 15px;
  left: 15px; 
  max-width: 250px;

  @media screen and (max-width: 1032px) {
    top: 10px;
  }

  @media screen and (max-width: 1024px) {
    max-width: 200px;
  }

  @media screen and (max-width: 625px) {
    grid-column: 1;
    grid-row: 3;
    max-width: 350px;
    position: static;
    padding: 10px;
    margin: 0 auto;
  }
`
const InterationDiv = styled.div`
  grid-column: 2;
  grid-row: 3;
  position: relative;
  left: 15px;
  max-width: 250px;

  @media screen and (max-width: 1024px) {
    max-width: 200px;
  }

  @media screen and (max-width: 625px) {
    grid-column: 1;
    grid-row: 4;
    max-width: 350px;
    position: static;
    padding: 10px;
    margin: 0 auto;
  }
`

const FinalProjectDiv = styled.div`
  grid-column: 3;
  grid-row: 3;
  position: relative;
  left: 25px;
  top: -60px;
  max-width: 250px;
  padding-top: 5px;

  @media screen and (max-width: 1215px) {
    top: -50px;
  }

  @media screen and (max-width: 1024px) {
    max-width: 200px;
  }

  @media screen and (max-width: 940px) {
    top: -45px;
  }

  @media screen and (max-width: 625px) {
    grid-column: 1;
    grid-row: 5;
    max-width: 350px;
    position: static;
    padding: 10px;
    margin: 0 auto;
  }
`

const FullStackDiv = styled.div`
  grid-column: 4;
  grid-row: 3;
  position: relative;
  left: 20px;
  top: -60px;
  max-width: 250px;
  padding-top: 5px;

  @media screen and (max-width: 1215px) {
    top: -50px;
    left: 30px;
  }

  @media screen and (max-width: 1024px) {
    max-width: 200px;
  }

  @media screen and (max-width: 940px) {
    top: -45px;
  }

  @media screen and (max-width: 670px) {
    top: -45px;
  }

  @media screen and (max-width: 625px) {
    grid-column: 1;
    grid-row: 6;
    max-width: 350px;
    position: static;
    padding: 10px;
    margin: 0 auto;
    margin-bottom: 150px;
  }
`

function Block08() {
  return (
    <Bg>
      <div className="container">
        <div className="columns is-marginless">
          <div className="column">
            <TitleDiv>
              <Title>
                <SizedTitle>C</SizedTitle>omo o
                <SizedTitle> C</SizedTitle>urso vai
                <SizedTitle> F</SizedTitle>uncionar
                <SizedTitle>?</SizedTitle>
              </Title>
            </TitleDiv>
          </div>
        </div>

        <Grid className="is-marginless is-multiline">
          <StepByStepDiv>
            <Subtitle style={{ color: "#00f7ff" }}>Passo a Passo</Subtitle>
            Você terá um <span className="has-text-weight-bold">plano de estudos: </span>
            um passo a passo dizendo quais
            aulas seguir do início ao fim e o
            que fazer para que em 8 semanas
            você se torne um Programador Full
            Stack JavaScript.
          </StepByStepDiv>

          <StepsImage className="image">
            <Image src={stepsImage} alt="steps" />
          </StepsImage>

          <ImediateAccess>
            <Subtitle style={{ color: '#fecb2f' }}>Acesso Imediato</Subtitle>
            Você receberá o acesso
            a todos os conteúdos do
            curso <span className="has-text-weight-bold">imediatamente em seu e-mail.</span>
          </ImediateAccess>

          <InterationDiv>
            <Subtitle style={{ color: '#00f7ff' }}>Interação</Subtitle>
            Você fará parte do &nbsp;
            <span className="has-text-weight-bold">
              grupo de discussão dos alunos.
            </span>&nbsp;
            Poderá
            troca experiências, fazer
            <span className="has-text-weight-bold">networking,</span> tirar dúvidas e
            evoluir junto com seus colegas.
          </InterationDiv>

          <FinalProjectDiv>
            <Subtitle style={{ color: '#f85d59' }}>Projeto Final</Subtitle>
            Você concluirá o curso ao
            desenvolver o nosso projeto final
            <span className="has-text-weight-bold"> um projeto inspirado no Evernote. </span>
            Dessa maneira você já vai para
            o mercado de trabalho com um
            excelente projeto em seu
            <span className="has-text-weight-bold"> portfólio!</span>
          </FinalProjectDiv>

          <FullStackDiv>
            <Subtitle style={{ color: '#694ed8' }}>
              Você Full Stack com Certificado!
            </Subtitle>
            Ao final de 8 semanas, após muita
            interação com o grupo e já tendo
            finalizado seu projeto inspirado no Evernote,
            você estará pronto para buscar seu primeiro
            job bom Programador Full Stack JavaScript
            Júnior!
            Como símbolo de sua conquista, você terá
            direito ao certificado de conclusão do curso
            após a entrega do projeto final.
            Mas lembre-se: o melhor certificado que
            você pode apresentar, é o seu conhecimento
            e suas novas habilidades adiquiridas!
          </FullStackDiv>

        </Grid>
      </div>
      <TrophieImage className="image">
        <img src={trophieImage} alt="trophie"/>
      </TrophieImage>
    </Bg>
  )
}

export default Block08