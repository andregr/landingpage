import React from 'react'
import styled from 'styled-components'
import { TitleDiv, Title, SizedTitle, SizedYellowTitle, Subtitle, Image, ParagraphBold, Paragraph } from '../StyledComponents'

import bgImage from '../../images/bg-pontinhos.png'
import pc from '../../images/pc.png'
import celular from '../../images/celular.png'

import quadroJavascript from '../../images/quadro-JS.png'
import quadroNode from '../../images/quadro-nodeJS.png'
import quadroReact from '../../images/quadro-react.png'
import quadroExpress from '../../images/quadro-express.png'

import tracejado from '../../images/tracejado-jobs.png'

import jobAmarelo from '../../images/job-amarelo.png'
import jobAzul from '../../images/job-azul.png'
import jobRoxo from '../../images/job-roxo.png'
import jobVermelho from '../../images/job-vermelho.png'

const Bg = styled.div`
  background-color: #251938;
  background-image: url(${bgImage});
`

const MobileHidden = styled.div`
  @media screen and (max-width: 768px) {
    display: block;
    visibility: hidden;
    position: fixed;
    top: -100;
  }
`

const TracejadoDiv = styled.div`
  position: relative;
  top: -25px;
`

const JobRoxo = styled.div`
  position: relative;
  top: -100px;

  @media screen and (max-width: 1215px) {
    top: -90px;
  }

  @media screen and (max-width: 1024px) {
    top: -90px;
    right: -30px;
  }

  @media screen and (max-width: 768px) {
    position: static;
  }
`

const JobVermelho = styled.div`
  position: relative;
  top: -150px;
  right: -50px;

  @media screen and (max-width: 1215px) {
    top: -120px;
    right: -50px;
  }

  @media screen and (max-width: 768px) {
    position: static;
  }
`

const JobAzul = styled.div`
  position: relative;
  top: -70px;
  right: -100px;

  @media screen and (max-width: 1215px) {
    top: -70;
  }

  @media screen and (max-width: 1215px) {
    top: -70px;
    right: -70px;
  }

  @media screen and (max-width: 768px) {
    position: static;
  }
`

const JobAmarelo = styled.div`
  position: relative;
  top: -140px;
  right: -150px;

  @media screen and (max-width: 1215px) {
    top: -100px;
  }

  @media screen and (max-width: 1215px) {
    top: -100px;
    right: -90px;
  }

  @media screen and (max-width: 768px) {
    position: static;
  }
`

const VagasDiv = styled.div`
  background-color: #694ed8;
  border-right: 5px solid #fecb2f;
  height: 40px;
  margin: 0px;
  padding: 5px;

  @media screen and (max-width: 768px) {
    width: 60%;
    margin-top: 20px;
  }
`

const ObsVagas = styled.p`
  color: white;
  font-size: 10px;
  margin-top: 0px;
  padding-top: 0px;
  text-align: right;

  @media screen and (max-width: 768px) {
    width: 60%;
    text-align: left;
  }
`

function Block04() {
  return (
    <Bg>
      <div className="container">
        <div className="columns is-marginless">
          <TitleDiv className="column">
            <Title>
              <SizedTitle>O</SizedTitle> que você vai
              <SizedYellowTitle> Realmente aprender</SizedYellowTitle>
            </Title>
            <Title>
              com este curso
              <SizedTitle>?</SizedTitle>
            </Title>
          </TitleDiv>
        </div>

        <div className="columns is-marginless">
          <div className="column is-6">
            <div className="columns">
              <Image src={pc} alt="pc" />
            </div>

            <div className="columns">
              <div className="column">
                <Subtitle>
                  Programação web
                </Subtitle>
                <ParagraphBold className="has-text-centered is-uppercase">
                  Do zero ao mercado de trabalho
                </ParagraphBold>
                <Paragraph className="has-text-centered">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vehicula ipsum id lectus semper, 
                  in auctor risus faucibus. Aliquam facilisis sapien ac ipsum ullamcorper ultricies. 
                  Ut condimentum mauris lectus, in consequat lacus porta in. Nulla vestibulum dui sed justo viverra, 
                  vel elementum ipsum vestibulum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, 
                  per inceptos himenaeos. Nulla vitae neque nec est finibus hendrerit in sed odio. 
                  Pellentesque quam eros, pharetra aliquam feugiat a, molestie eget turpis.
                  Mauris feugiat varius sollicitudin. Integer aliquet, ante at fringilla dictum, 
                  ipsum ligula facilisis turpis
                </Paragraph>
              </div>
            </div>
            
          </div>

          <div className="column is-6">
            <div className="columns" style={{paddingTop: '12px'}}>
                <Image src={celular} alt="mobile" />
              </div>

              <div className="columns">
                <div className="column">
                  <Subtitle>
                    Uma profissão
                  </Subtitle>
                  <ParagraphBold className="has-text-centered is-uppercase">
                    De hoje e do futuro!
                  </ParagraphBold>
                  <Paragraph className="has-text-centered">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vehicula ipsum id lectus semper, 
                    in auctor risus faucibus. Aliquam facilisis sapien ac ipsum ullamcorper ultricies. 
                    Ut condimentum mauris lectus, in consequat lacus porta in. Nulla vestibulum dui sed justo viverra, 
                    vel elementum ipsum vestibulum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, 
                    per inceptos himenaeos. Nulla vitae neque nec est finibus hendrerit in sed odio. 
                    Pellentesque quam eros, pharetra aliquam feugiat a, molestie eget turpis.
                    Mauris feugiat varius sollicitudin. Integer aliquet, ante at fringilla dictum, 
                    ipsum ligula facilisis turpis
                  </Paragraph>
                </div>
              </div>
          </div>
        </div>

        <div className="columns is-marginless">
          <div className="column is-8 is-offset-2">
            <TitleDiv>
              <Title>
                <SizedTitle>V</SizedTitle>ocê vai
                <SizedYellowTitle> Dominar </SizedYellowTitle>
                as tecnologias
              </Title>
              <Title style={{ paddingTop: '5px'}}>
                mais <SizedTitle>Modernas</SizedTitle> e
                <SizedTitle> Perdidas</SizedTitle> pelas
                <SizedTitle> S</SizedTitle>tartups
              </Title>
            </TitleDiv>
          </div>
        </div>

        <div className="columns is-marginless">
          <div className="column">
            <Image src={quadroJavascript} alt="javascript block"/>
          </div>
          
          <div className="column">
            <Image src={quadroNode} alt="nodeJS block"/>
          </div>
          
          <div className="column">
            <Image src={quadroReact} alt="react block"/>
          </div>
          
          <div className="column">
            <Image src={quadroExpress} alt="express block"/>
          </div>
        </div>

        <MobileHidden className="columns is-marginless">
          <TracejadoDiv className="column is-10 is-offset-1 image">
            <Image src={tracejado} alt="tracejado"/>
          </TracejadoDiv>
        </MobileHidden>

        <div className="columns is-marginless is-paddingless">
          <JobRoxo className="column is-2 is-offset-1">
            <Image src={jobRoxo} alt="job nodeJS"/>
          </JobRoxo>
          
          <JobVermelho className="column is-2">
            <Image src={jobVermelho} alt="job express"/>
          </JobVermelho>
          
          <JobAzul className="column is-2">
            <Image src={jobAzul} alt="job react"/>
          </JobAzul>
          
          <JobAmarelo className="column is-2">
            <Image src={jobAmarelo} alt="job javascrip"/>
          </JobAmarelo>
        </div>

        <div className="columns is-marginless">
          <VagasDiv className="column is-5">
            <Title className="has-text-right">
              Vagas Reais<SizedTitle>!</SizedTitle>
            </Title>
          </VagasDiv>
        </div>

        <div className="columns is-marginless">
          <div className="column is-5">
            <ObsVagas>*vagas reais encontradas no site https://programathor.com.br em junho de 2019</ObsVagas>
          </div>
        </div>

      </div>
    </Bg>
  )
}

export default Block04;