import React from 'react'
import styled from 'styled-components'
import DropdownList from './dropdown-list'
import PropTypes from 'prop-types'
import setaEscura from '../../images/seta-roxo-escuro-12.png'

const Box = styled.div`
  display: block;
  color: #251938;
  background-color: #fff;
  width: 75%;
  font-weight: 600;
  padding: 0px 20px 10px 0px;
  box-shadow: 7px 7px 0px rgba(0, 0, 0, 0.4);
  padding-left: 10px;
  position: relative;
  margin-top: 6px;
  margin: 0 auto;
  max-width: 432px;
  z-index: 1;
`

const Paragraph = styled.p`
  line-height: ${text => text.children.length > 20 ? 25 : 40}px;
  padding-top: ${text => text.children.length > 30 ? 2 : 6}px;
  font-size: ${text => text.children.length > 20 ? 20 : 30}px;
  max-width: 95%;
  user-select: none;
  cursor: pointer;

  @media screen and (max-width: 1407px) {
    font-size: 20px;
    line-height: 30px;
    margin-top: 5px;
    max-width: 90%;
  }

  @media screen and (max-width: 940px) {
    font-size: 18px;
    line-height: 20px;
  }

  @media screen and (max-width: 876px) {
    font-size: 14px;
  }
`

const Image = styled.img`
  position: absolute;
  right: 12px;
  top: 35%;
  max-height: 17px;
  max-width: 28px;

  @media screen and (max-width: 876px){
    width: 20px !important;
  }
`

/**
 * Component to render the dropdown-extras
 * @param {number} number
 *  Number of the dropdown used to set some specific css style between dropdown and dropdown-extras items
 * @param {string} label
 *  Label of dropdown
 * @param {array} items
 *  Array of objects ({label: string, duration: string}) used as list to the dropdown
 * @param {function} handleOnClick
 *  Function used to show or hide the dropdown-items
 */
function DropdownExtras({number, label, items, handleOnClick}) {
  return(
    <>
      <Box onClick={() => handleOnClick(number)}>
        <Paragraph>{label}</Paragraph>
        <Image src={setaEscura} alt="arrow"/>
      </Box>
      <div id={`dropdown-items-${number}`} className="is-hidden">
        <DropdownList
          number={number}
          items={items} />
      </div>
    </>
  )
}

DropdownExtras.prototype = {
  number: PropTypes.number.isRequired,
  label: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      duration: PropTypes.string.isRequired,
    }),
  ),
  handleOnClick: PropTypes.func.isRequired,
}

export default DropdownExtras