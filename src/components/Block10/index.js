import React from 'react'
import styled from 'styled-components'

import { TitleDiv, Title, SizedTitle, YellowTitle, Image, WhiteText } from '../StyledComponents'

import bgImage from '../../images/bg-10.png'
import gifImage from '../../images/gif-10.png'
import logosImage from '../../images/logos-jsnotes-evernote-10.png'

const Bg = styled.div`
  width: 100%;
  background-color: #694ed8;
  background-image: url(${bgImage});
`

const Ul = styled.ul`
  font-size: 16px;
  color: #fff;
  list-style: disc;
`

function Block10() {
  return (
    <Bg>
      <div className="container">
        <div className="columns is-marginless">
          <div className="column">
            <TitleDiv>
              <Title> 
                <SizedTitle>P</SizedTitle>rojeto final: 
                <YellowTitle>
                  <SizedTitle> App</SizedTitle> inspirado no <SizedTitle>E</SizedTitle>vernote
                </YellowTitle>
              </Title>
              <Title style={{ marginTop: '10px'}}>
                <SizedTitle>[</SizedTitle>
                Direto para seu <SizedTitle>P</SizedTitle>ortfólio
                <SizedTitle>!]</SizedTitle>
              </Title>
            </TitleDiv>
          </div>
        </div>

        <div className="columns is-marginless">
          <div className="column">
            <Image src={gifImage} alt="gif"/>
            <Image src={logosImage} alt="logos" style={{ marginTop: '15px'}}/>
          </div>

          <div className="column">
            <WhiteText>
              Ao final do curso, você desenvolverá um App inspirado no <span className="has-text-weight-bold">Evernote, </span>
              um software muito famoso destinado a organização de informações
              com um arquivo de notas.
            </WhiteText>

            <WhiteText>
              Você pode saber mais sobre o Evernote no site <span className="is-italic">https://evernote.com.</span>
            </WhiteText>

            <WhiteText style={{ marginTop: '15px'}}>
              O noso APP será o JavaScript<span className="has-text-weight-bold">Notes</span> e terá a mesma função: criar,
              salvar e armazenar notas. Você ainda poderá editar suas notas e
              excluir se desejar. Além disso, faremos o login para que o usuário
              consiga acessar suas notas quando quiser.
            </WhiteText>

            <WhiteText style={{ marginTop: '15px'}}>
              Nosso stack (conjunto de ferramentas) para este projeto será:
            </WhiteText>
            
            <div className="columns is-marginless is-mobile">
              <div className="column is-5" style={{ paddingLeft: '30px', width: '180px'}}>
                <Ul>
                  <li>JavasScript</li>
                  <li>React</li>
                </Ul>
              </div>

              <div className="column is-5">
                <Ul>
                  <li>Express</li>
                  <li>Bulma</li>
                </Ul>
              </div>
            </div>

            <WhiteText style={{marginTop: '15px'}}>
              Durante o desenvolvimento, você entenderá a importancia do planejamento,
              mockups, dominará banco de dados, outra tecnologia e outra tecnologia.
            </WhiteText>

            <WhiteText style={{marginTop: '15px', marginBottom: '30px'}}>
              Você passará por todas as etapas de desenvolvimento de um APP
              profissional e estará preparado para o mercado de trabalho.
            </WhiteText>
          </div>
        </div>
      </div>
    </Bg>
  )
}

export default Block10