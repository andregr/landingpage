import React from 'react'
import styled from 'styled-components'
import { TitleDiv, Title, SizedTitle, YellowTitle, SizedYellowTitle, Text, SpanBold, Image } from '../StyledComponents'

import bg from '../../images/bg-5.png'
import avioes from '../../images/avioes.png'
import calendario from '../../images/calendario.png'
import logo from '../../images/logo-onebitcode.png'
import networking from '../../images/networking.png'
import pc from '../../images/pc-block05.png'
import tablet from '../../images/tablet.png'

const Bg = styled.div`
  background-color: #694ed8;
  background-image: url(${bg});
`

const YellowDiv = styled.div`
  background-color: #fecb2f;
  color: #694ed8;
  text-align: center;
  height: 39px;
  width: 226px;
  margin: 0 auto;
  padding: 5px;
  font-weight: 600;
  font-size: 20px;
`

const SizedText = styled.span`
  color: white;
  font-size: 24px;

  @media screen and (max-width: 568px){
    font-size: 20px;
  }
`

const NormalText = styled.p`
  color: white;
  font-size: 20px;
  text-align: justify;
  padding-bottom: 15px;

  @media screen and (max-width: 568px){
    font-size: 16px;
  }
`

function Block05() {
  return (
    <Bg>
      <div className="container">
        <div className="columns is-marginless">
          <TitleDiv className="column">
            <Title>
              <SizedTitle>P</SizedTitle>or que aprender com a
              <YellowTitle>
                <SizedYellowTitle> E</SizedYellowTitle>scola de
                <SizedYellowTitle> J</SizedYellowTitle>ava
                <SizedYellowTitle>S</SizedYellowTitle>cript
              </YellowTitle>
            </Title>
            <Title style={{ paddingTop: '5px' }}>
              e <SizedTitle>Não</SizedTitle> com outro curso<SizedTitle>?</SizedTitle>
            </Title>
          </TitleDiv>
        </div>

        <div className="columns is-marginless">
          <div className="column">
            <div className="columns is-multiline">
              <div className="column is-12" style={{ height: '190px' }}>
                <Image src={calendario} alt="calendar" style={{ padding: '5% 0' }} />
              </div>

              <div className="column is-12">
                <YellowDiv>
                  Plano de Estudos
                </YellowDiv>

                <Text>
                  Você será direcionado para que<SpanBold> em até 8 semanas </SpanBold>
                  consiga se tornar um Programaodr Full Stack e possa ir em busca de seu primeiro job.
                  Ao contrário dos cursos comuns nós vamos <SpanBold>te dar todo o passo a passo </SpanBold>
                  e você saberá <SpanBold> o que</SpanBold> estudar,
                  <SpanBold> quando </SpanBold>estudar e <SpanBold> como </SpanBold>aplicar.
                </Text>

                <SpanBold>Não te deixaremos sozinho nessa!</SpanBold>
              </div>

            </div>
          </div>

          <div className="column">
            <div className="columns is-multiline">
              <div className="column is-12" style={{ height: '190px' }}>
                <Image src={tablet} alt="tablet" style={{ padding: '10% 0' }} />
              </div>

              <div className="column is-12">
                <YellowDiv style={{ fontSize: '12px', padding: '3px' }}>
                  Conteúdo extras com quem já tem experiência
                </YellowDiv>

                <Text>
                  Além de todo material técnico,
                  você vai ter acesso a <SpanBold>entrevitas exclusivas </SpanBold>
                  com diversos programadores que já atuam na área.
                  Você vai conhecer a profissão
                  com a visão de quem já tem
                  experiência e pode falar como
                  tudo acontece no mundo real.
                  Entenderá as possibilidades de
                  trabalho e <SpanBold>como conseguir jobs </SpanBold>
                  CLT, PJ, Freelance e até mesmo
                  remoto (home-office).
                  Você <SpanBold>dominará </SpanBold> a <SpanBold>técnica</SpanBold>, a <SpanBold>teoria</SpanBold>,
                  a <SpanBold>prática</SpanBold> e terá <SpanBold>visão de mercado</SpanBold>.
                </Text>
              </div>


            </div>
          </div>

          <div className="column">
            <div className="columns is-multiline">
              <div className="column" style={{ height: '190px' }}>
                <Image src={networking} alt="networking" />
              </div>

              <div className="column is-12">
                <YellowDiv>
                  Networking
                </YellowDiv>

                <Text>
                  Você se juntará a <SpanBold>dezenas</SpanBold> de
                  outros programadores num <SpanBold>grupo de discussão do curso </SpanBold>
                  e poderá compartilhar suas dúvidas e suas
                  conquistas, ajudar e ser ajudado,
                  trocar experiências e <SpanBold>ficar de olho nos jobs </SpanBold>
                  que a galera posta por lá.
                </Text>
              </div>
            </div>
          </div>

          <div className="column">
            <div className="columns is-multiline">
              <div className="column" style={{ height: '190px' }}>
                <Image src={pc} alt="pc" style={{ padding: '5% 0' }} />
              </div>

              <div className="column is-12">
                <YellowDiv style={{ fontSize: '12px', padding: '3px' }}>
                  Portfólio ativo e com um projeto real desenvolvido
                </YellowDiv>

                <Text>
                  Para concluir com sucesso o curso,
                  você desenvolverá um <SpanBold>projeto final inspirado no APP Evernote </SpanBold>
                  (software destinado a organização da informação pessoal mediante um arquivo de notas).
                  O projeto ficará publicado em seu
                  <SpanBold> Github </SpanBold> e será a porta de entrada para
                  os melhores jobs como Programador Full Stack.
                </Text>
              </div>

            </div>
          </div>
        </div>

        <div className="columns" style={{ margin: '30px 0px 0px 0px'}}>
          <div className="column is-offset-3 is-6">
              <SizedText>
                A <span className="has-text-weight-semibold">Escola de Javascript</span> é iniciativa da
                <span className="has-text-weight-semibold"> startup de educação online OneBitCode</span>
              </SizedText>
            </div>

            <div className="column is-3">
              <Image src={logo} alt="onebitcode logo"/>
            </div>
        </div>

        <div className="columns is-marginless">
          <div className="column is-3">
            <Image src={avioes} alt="planes"/>
          </div>

          <div className="column is-9">
            <NormalText>
              O OneBitCode foi criado em 2016 e desde então vem ajudando
              <span className="has-text-weight-semibold"> milhares de programadores </span>
              pelo Brasil e pelo mundo.
            </NormalText>

            <NormalText >
              Pensando em trazer a <span className="has-text-weight-semibold"> qualidade dos cursos </span>
              OneBitCode para o mundo JavaScript, nasce a Escola de JavaScript, uma escola
              <span className="has-text-weight-semibold"> 100% online </span> que leva até o programador
              <span className="has-text-weight-semibold"> conteúdos atuais, </span>aprendizado
              <span className="has-text-weight-semibold"> rápido e eficiente </span> e o maior diferencial de todos:
              todo aprendizado é feito com base em <span className="has-text-weight-semibold"> projetos, </span>
              ou seja, você sempre vai aprender desenvolvendo uma aplicação de verdade, passando por todas as fases
              necessárias que um programador deve dominar.
            </NormalText>

            <NormalText>
              Acreditamos que <span className="has-text-weight-semibold"> colocar a mão na massa </span> 
              é o melhor jeito de aprender a desenvolver suas habilidades com sucesso!
            </NormalText>

            <NormalText>
              Então, aproveite que você conheceu um pouquinho de nossa história e veja os
              <span className="has-text-weight-semibold"> artigos </span> e 
              <span className="has-text-weight-semibold"> screencasts </span>
              gratuitos em nosso blog para entender nosso jeito de ensinar e como você vai aprender.
            </NormalText>

            <NormalText className="has-text-weight-semibold is-paddingless">
              Não perca mais tempo, inscreva-se no curso
            </NormalText>
            
            <NormalText className="has-text-weight-semibold" style={{ marginBottom: '30px'}}>
              Programador Full Stack JavaScript em 8 semanas e faça sua história também!
            </NormalText>
          </div>
        </div>
      </div>
    </Bg>
  )
}

export default Block05