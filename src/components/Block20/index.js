import React from 'react'
import styled from 'styled-components'

import { TitleDiv, Title, YellowTitle, SizedTitle, Paragraph20, Image } from '../StyledComponents'

import bgImage from '../../images/bg-20.png'
import openQuotes from '../../images/abre-aspas-20.png'
import closeQuotes from '../../images/fecha-aspas-20.png'

const Bg = styled.section`
  width: 100%;
  background-color: #694ed8;
  background-image: url(${bgImage});
  padding-bottom: 40px;
`

const WhiteDiv = styled.div`
  background-color: rgba(255, 255, 255, 0.8);
  border-radius: 20px;
  box-shadow: -6px 8px rgba(0, 0, 0, 0.2);
  margin-top: 30px;
  font-size: 18px;
  font-weight: 600;
  font-style: italic;
  padding: 20px 30px;
  color: #694ed8;

  margin-left: ${props => props.right ? 'auto' : '20px'};
  margin-right: ${props => props.right ? '20px' : 'auto'}; 

  @media screen and (max-width: 768px) {
    margin: 0 auto;
  }
`

const Paragraph = styled.p`
  font-size: 14px;
  color: #251938;
  text-align: right;
  margin-top: 20px;
  font-style: normal;
`

const CloseQuotesDiv = styled.div`
  position: relative;
  bottom: -180px;
  max-height: 75px;

  @media screen and (max-width: 1215px) {
    bottom: -210px;
  }

  @media screen and (max-width: 890px) {
    bottom: -240px;
  }

  @media screen and (max-width: 768px) {
    bottom: 0px;
    margin-top: 20px;
  }
`

const Footer = styled.footer`
  margin-top: 50px;
  font-size: 14px;
  color: white;
`

const YellowText = styled.span`
  color: #fecb2f;
  font-weight: 700;
`

function Block20() {
  return (
    <Bg>
      <div className="container">
        <div className="columns">
          <div className="column">
            <TitleDiv>
              <Title>
                <YellowTitle>
                  <SizedTitle>D</SizedTitle>epoimentos dos <SizedTitle>A</SizedTitle>lunos
                </YellowTitle>
              </Title>
            </TitleDiv>
          </div>
        </div>

        <div className="columns">
          <div className="column">
            <Paragraph20 style={{ border: 'none', fontWeight: 'normal'}}>
              O curso Programador Full Stack JavaScript em 8 semanas <span className="has-text-weight-bold">é novo e ainda não possui comentários,</span> então aqui em
              baixo listamos <span className="has-text-weight-bold">comentários sobre outros conteúdos</span> da <span className="has-text-weight-bold">Escola de JavaScript</span> e do 
              <span className="has-text-weight-bold"> OneBitCode</span> para que você veja o que o pessoal acha do nosso jeito de ensinar! :)
            </Paragraph20>
          </div>
        </div>

        <div className="columns">
          <div className="column is-1">
            <Image src={openQuotes} alt="open quotes"/>
          </div>

          <WhiteDiv className="column is-8" right={false}>
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent viverra justo sed tellus ullamcorper 
            efficitur. Ut lacinia hendrerit volutpat. Quisque pulvinar mauris diam"

            <Paragraph>
              Comentários sobre o post "Introdução prática ao AdonisJS: Criando um APP de enventos"
            </Paragraph>

            <Paragraph style={{ marginTop: '0'}}>
              Escola de Javascript.
              <a href="#" className="has-text-weight-bold" style={{ color: "#251938"}}> 
                &nbsp;&nbsp; acesse clicando aqui :)
              </a>
            </Paragraph>
          </WhiteDiv>

        </div>

        <div className="columns">
          <WhiteDiv className="column is-8" right={true}>
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent viverra justo sed tellus ullamcorper 
            efficitur. Ut lacinia hendrerit volutpat. Quisque pulvinar mauris diam"

            <Paragraph>
              Comentários sobre o post "Introdução prática ao AdonisJS: Criando um APP de enventos"
            </Paragraph>

            <Paragraph style={{ marginTop: '0'}}>
              Escola de Javascript.
              <a href="#" className="has-text-weight-bold" style={{ color: "#251938"}}> 
                &nbsp;&nbsp; acesse clicando aqui :)
              </a>
            </Paragraph>
          </WhiteDiv>
        </div>

        <div className="columns">
          <WhiteDiv className="column is-8 is-offset-1" right={false}>
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent viverra justo sed tellus ullamcorper 
            efficitur. Ut lacinia hendrerit volutpat. Quisque pulvinar mauris diam"

            <Paragraph>
              Comentários sobre o post "Introdução prática ao AdonisJS: Criando um APP de enventos"
            </Paragraph>

            <Paragraph style={{ marginTop: '0'}}>
              Escola de Javascript.
              <a href="#" className="has-text-weight-bold" style={{ color: "#251938"}}> 
                &nbsp;&nbsp; acesse clicando aqui :)
              </a>
            </Paragraph>
          </WhiteDiv>
        </div>

        <div className="columns">
          <WhiteDiv className="column is-8" right={true}>
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent viverra justo sed tellus ullamcorper 
            efficitur. Ut lacinia hendrerit volutpat. Quisque pulvinar mauris diam"

            <Paragraph>
              Comentários sobre o post "Introdução prática ao AdonisJS: Criando um APP de enventos"
            </Paragraph>

            <Paragraph style={{ marginTop: '0'}}>
              Escola de Javascript.
              <a href="#" className="has-text-weight-bold" style={{ color: "#251938"}}> 
                &nbsp;&nbsp; acesse clicando aqui :)
              </a>
            </Paragraph>
          </WhiteDiv>

          <CloseQuotesDiv className="column is-1">
            <Image src={closeQuotes} alt="close quotes"/>
          </CloseQuotesDiv>
        </div>

        <div className="columns has-text-centered">
          <div className="column">
            <Footer>
              <p className="has-text-weight-bold">Escola de Javascript</p>
              <p>Feito com <YellowText>s2</YellowText> por <YellowText>OneBitCode.com</YellowText></p>
            </Footer>
          </div>
        </div>

      </div>
    </Bg>
  )
}

export default Block20