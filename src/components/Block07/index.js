import React from 'react'
import styled from 'styled-components'

import { TitleDiv, PurpleTitle, SizedTitle } from '../StyledComponents'

import interrogationImage from '../../images/interrogacao-7.png'
import arrowImage from '../../images/seta-7.png'

const Bg = styled.div`
  width: 100%;
  background-color: #fecb2f;
  background-image: url(${interrogationImage});
  background-repeat: no-repeat;
  background-position: center center;
`

const TextDiv = styled.div`
  margin-bottom: 10px;
`

const Text = styled.div`
  margin-left: 10px;
  font-size: 16px;
  font-weight: 600;
  position: relative;
  top: -3px;
  display: inline-block;

  @media screen and (max-width: 510px) {
    display: inline;
  }
`

const PurpleText = styled.span`
  background-color: #694ed8;
  color: #fff;
`

const Image = styled.img`
  vertical-align: top;
`

function Block07() {
  return (
    <Bg>
      <div className="container">
        <div className="columns is-marginless">
          <div className="column">
            <div className="columns is-marginless is-multiline">
              <div className="column">
                <TitleDiv>
                  <PurpleTitle>
                    <SizedTitle>Q</SizedTitle>uem pode se inscrever <br/>
                    nesse curso <SizedTitle>?</SizedTitle>
                  </PurpleTitle>
                </TitleDiv>
                
                <TextDiv>
                  <Image src={arrowImage} alt="purple arrow" />
                  <Text>
                    Quem deseja <PurpleText>começar</PurpleText> uma profissão nova
                  </Text>
                </TextDiv>

                <TextDiv>
                  <Image src={arrowImage} alt="purple arrow" />
                  <Text>
                    Quem <PurpleText>não sabe NADA</PurpleText> de programação web
                  </Text>
                </TextDiv>

                <TextDiv>
                  <Image src={arrowImage} alt="purple arrow"/>
                  <Text>
                    Quem <PurpleText>já tem uma noção</PurpleText> de Lógica de Programação
                  </Text>
                </TextDiv>

                <TextDiv>
                  <Image src={arrowImage} alt="purple arrow"/>
                  <Text>
                    Quem já consegue se virar com os códigos, mas <br/>
                    <PurpleText> quer dar aquele UPGRADE</PurpleText> no conhecimento
                  </Text>
                </TextDiv>

                <TextDiv>
                  <Image src={arrowImage} alt="purple arrow" />
                  <Text>
                    Quem domina o JavaScript, mas deseja REFORÇAR <br/>
                    a base e <PurpleText>conhecer NOVOS MÉTODOS</PurpleText>
                  </Text>
                </TextDiv>
              </div>
            </div>
          </div>
          
          <div className="column">
            <div className="columns is-marginless is-multiline">
              <div className="column">
                <TitleDiv>
                  <PurpleTitle>
                    <SizedTitle>Q</SizedTitle>uais são os <br/>
                    <SizedTitle>P</SizedTitle>ré-<SizedTitle>R</SizedTitle>equisitos 
                    <SizedTitle>T</SizedTitle>écnicos <SizedTitle>?</SizedTitle>
                  </PurpleTitle>
                </TitleDiv>

                <TextDiv>
                  <Image src={arrowImage} alt="purple arrow" />
                  <Text>
                    Ter acesso a um computador
                  </Text>
                </TextDiv>

                <TextDiv>
                  <Image src={arrowImage} alt="purple arrow" />
                  <Text>
                    Ter acesso à internet
                  </Text>
                </TextDiv>

                <TextDiv>
                  <Image src={arrowImage} alt="purple arrow" />
                  <Text>
                    Saber o básico para acessar sites, vídeos, <br/>
                    documentos, etc.
                  </Text>
                </TextDiv>

              </div>
            </div>
          </div>
        </div>
      </div>
    </Bg>
  ) 
}

export default Block07