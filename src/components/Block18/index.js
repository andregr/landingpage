import React from 'react'
import styled from 'styled-components'

import { TitleDiv, PurpleTitle, SizedTitle } from '../StyledComponents'

import square from '../../images/quadrado-pagina-18.png'
import people from '../../images/pessoas-18.png'

const Bg = styled.section`
  width: 100%;
  position: relative;
  background-color: #fecb2f;
  padding: 0 0 40px 0;
  margin: 0;
`

const ImageDiv = styled.div`
  position: absolute;
  top: 0%;
  left: 0%;
  max-height: 86px;
  max-width: 86px;
`

const FoldImage = styled.img`
  display: block;
  margin: 0;
  padding: 0;

  @media screen and (max-width: 767px){
    max-height: 43px;
    max-width: 43px;
  }
`

const PeopleDivColumns = styled.div`
  padding-top: 40px;

  @media screen and (max-width: 980px)  {
    padding-top: 0%;
  }
  
`

const PeopleDiv = styled.div`
  display: block; 
  position: absolute;
  top: 58%;
  right: 0%;

  @media screen and (max-width: 980px) {
    position: static;
  }
`

const PeopleImage = styled.img`
  display: block;
  margin: 0;
  margin: 0 auto;
  max-width: 275px;
  max-height: 160px;
`

const Paragraph = styled.p`
  font-size: 22px;
  line-height: 1.4em;
  color: #251938;

  @media screen and (max-width: 767px) {
    font-size: 16px;
  }
`

function Block18() {
  return (
    <Bg>
      <ImageDiv className="image">
        <FoldImage src={square} alt="square"/>
      </ImageDiv>
      <div className="container">
        <div className="columns">
          <div className="column">
            <TitleDiv>
              <PurpleTitle>
                para <SizedTitle>E</SizedTitle>mpresas
              </PurpleTitle>
            </TitleDiv>
          </div>
        </div>

        <div className="columns has-text-centered">
          <div className="column">
            <Paragraph>
              Se você tem uma empresa/startup ou tem sua equipe de confiança e quer levar
              mais conhecimento para o seu peossoal, <span className="has-text-weight-bold">entre em contato</span> conosco e saiba mais sobre
              <span className="has-text-weight-bold"> condições especiais</span> e pacotes para <span className="has-text-weight-bold">mais de 10 acessos.</span>
            </Paragraph>
          </div>
        </div>

        <div className="columns has-text-centered">
          <div className="column">
            <Paragraph>
              <span className="has-text-weight-bold">contato@escoladejavascript.com</span>
            </Paragraph>
            
            <Paragraph>
              19 99709-5421 . 11 98958-4073
            </Paragraph>
          </div>
        </div>

        <PeopleDivColumns className="columns">
          <PeopleDiv className="column">
            <PeopleImage src={people} alt="people" />
          </PeopleDiv>
        </PeopleDivColumns>
      </div>

      
    </Bg>
  )
}

export default Block18