import React from 'react'
import styled from 'styled-components'

import { FloatingImgClickHere, FloatingImgSubscribe } from '../StyledComponents'

import bgImage from '../../images/bg-6.png'
import clickHere from '../../images/clique aqui-6.png'
import button from '../../images/botao-6.png'

const Bg = styled.div`
  width: 100%;
  background-color: #00f7ff;
  background-image: url(${bgImage});
`

function Block06() {
  return(
    <Bg>
      <div className="container">
        <div className="columns is-marginless" style={{ padding: '30px 0px 50px 5px'}}>
          <div className="column is-6 is-offset-3">
              <FloatingImgClickHere className="image">
                <img src={clickHere} alt="click here" />
              </FloatingImgClickHere>
              <FloatingImgSubscribe className="image">
                <a href="#">
                  <img src={button} alt="subscribe button" />
                </a>
              </FloatingImgSubscribe>
            </div>
        </div>
      </div>
    </Bg>
  )
}

export default Block06