import React from 'react';
import styled from 'styled-components'
import PropTypes from 'prop-types'

const Bg = styled.div`
  background-color: #251938;
  color: #FFF;
  max-width: 550px;
  margin: 0 10px;
  padding: 0 20px;
  display: block;
  padding: 40px 10px 10px 10px;
  box-shadow: 7px 7px 0px rgba(0, 0, 0, 0.4);
  position: relative;
  top: ${props => props.number > 8 ? -30 : -70}px;
  height: auto;

  @media screen and (max-width: 876px) {
    top: ${props => props.number > 8 ? -20 : -55}px;
  }
`

const Li = styled.li`
  background-color: ${props =>  props.index % 2 === 0 ? '#332744' : 'transparent'};
  display: flex;
  justify-content: space-between;
  vertical-align: middle;
`

const Paragraph = styled.p`
  font-size: ${props => props.children[2].length > 38 ? 16 : 20}px;
  color: #FFF;
  font-weight: 600;
  margin-left: 10px;
  flex: 1 1;
  text-align: left;

  @media screen and (max-width: 1215px) {
    font-size: ${props => props.children[2].length > 38 ? 14 : 20}px;
  }

  @media screen and (max-width: 890px) {
    font-size: 14px;
  }
`

const Duration = styled.p`
  color: #FFF;
  flex: 0 1;
  padding-right: 10px;
  padding-top: 3px;
`

/**
 * Component to render the items of dropdown and dropdown-extras
 * @param {array} items
 *  Array of objects ({label: string, duration: string}) used as list to the dropdown
 * @param {number} number
 *  Number of the dropdown to determine some specific styles between dropdown and dropdown-extras
 */
function DropdownItems({items, number}) {
  return (
    <Bg number={number}>
      <ul>
        {
          items.map((item, index) => (
            <Li key={`${number}-${index}`} index={index+1}>
              <Paragraph>{index + 1}. {item.label}</Paragraph>
              <Duration>{item.duration}</Duration>
            </Li>
          ))
        }
      </ul>
    </Bg>
  )
}

DropdownItems.prototype = {
  number: PropTypes.number.isRequired,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      duration: PropTypes.string.isRequired,
    }),
  ),
}

export default DropdownItems