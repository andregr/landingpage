import React from 'react'
import styled from 'styled-components'
import DropdownList from './dropdown-list'
import PropTypes from 'prop-types'

import setaClara from '../../images/seta-roxo-12.png'

const Number = styled.span`
  display: inline-block;
  color: #fecb2f;
  font-size: 80px;
  font-weight: 600;
  text-shadow: 7px 7px 0px rgba(0, 0, 0, 0.4);
  padding: 10px 20px;
  vertical-align: middle;
  width: 90.64px;
  position: relative;
  z-index: 1;

  @media screen and (max-width: 876px) {
    font-size: 60px;
    padding: 10px;
    width: 55.94px;
  }
`
const Box = styled.span`
  display: inline-block;
  color: #694ed8;
  background-color: #fff;
  width: 75%;
  font-weight: 600;
  padding: 0px 20px 10px 0px;
  box-shadow: 7px 7px 0px rgba(0, 0, 0, 0.4);
  vertical-align: middle;
  padding-left: 10px;
  position: relative;
  max-height: 50px;
  margin-top: 6px;
  margin: 0 auto;
  max-width: 432px;
  max-height: 50px;
  z-index: 1;
  text-align: left;
  min-height: 50px;
`

const Paragraph = styled.p`
  line-height: ${text => text.children.length > 20 ? 25 : 40}px;
  padding-top: ${text =>  text.children.length > 30 ? 2 : text.children.length > 20 ? 12 : 6}px;
  padding-bottom: ${text => text.children.length > 20 ? 0 : 0}px;
  font-size: ${text => text.children.length > 20 ? 20 : 30}px;
  max-width: 95%;
  user-select: none;
  cursor: pointer;

  @media screen and (max-width: 1215px) {
    font-size: 18px;
    line-height: 25px;
    margin-top: 0px;
    max-width: 90%;
    padding-bottom: 0px;
    padding-top: ${text => text.children.length > 30 ? 0 :  text.children.length > 20 ? 12 : 12}px;
  }

  @media screen and (max-width: 940px) {
    font-size: ${text => text.children.length > 20 ? 16 : 18}px;
    padding-top: ${text => text.children.length > 30 ? 0 :  text.children.length > 20 ? 9 : 10}px;
    line-height: 20px;
    margin-top: 5px;
  }

  @media screen and (max-width: 876px) {
    font-size: ${text => text.children.length > 30 ? 12 : 14}px;
    padding-top: ${text => text.children.length > 20 ? 4 : 10}px;
  }
`

const Image = styled.img`
  position: absolute;
  right: 12px;
  top: 35%;

  @media screen and (max-width: 876px){
    width: 20px;
  }
`

/**
 * Component to render the dropdown
 * @param {number} number
 *  Number of the dropdown used to set some specific css style between dropdown and dropdown-extras items
 * @param {string} label
 *  Label of dropdown
 * @param {array} items
 *  Array of objects ({label: string, duration: string}) used as list to the dropdown
 * @param {function} handleOnClick
 *  Function used to show or hide the dropdown-items
 */
function Dropdown({number, label, items, handleOnClick}) {
  return(
    <div className="column is-paddingless is-marginless" style={{ padding: '0', margin: '0 auto'}}>
      <div onClick={() => handleOnClick(number)}>
        <Number>{number}</Number>
        <Box>
          <Paragraph>{label}</Paragraph>
          <Image src={setaClara} alt="arrow" />
        </Box>
      </div>
      <div id={`dropdown-items-${number}`} className="is-hidden">
        <DropdownList
          number={number}
          items={items} />
      </div>
    </div>
  )
}

Dropdown.prototype = {
  number: PropTypes.number.isRequired,
  label: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      duration: PropTypes.string.isRequired,
    }),
  ),
  handleOnClick: PropTypes.func.isRequired,
}

export default Dropdown