import React from 'react'
import styled from 'styled-components'
import Dropdown from './dropdown';
import DropdownExtras from './dropdown-extras';

import { TitleDiv, Title, SizedTitle, YellowBlock } from '../StyledComponents'

import bgImage from '../../images/bg-12.png'
import bgExtras from '../../images/bg-extras12.png'

const Bg = styled.section`
  width: 100%;
  background-color: #694ed8;
  background-image: url(${bgImage});
  padding-bottom: 40px;
`
const BgExtras = styled.section`
  background: url(${bgExtras}) no-repeat center top;
  margin: 0 auto;
  padding-top: 55px;
  margin-top: 30px;
  display: block;
  min-height: 261px;
`

const items = [
  {label: 'O que é Algoritmo', duration: '2:56'},
  {label: 'Condicionais', duration: '8:20'},
  {label: 'Estrutura de repetição', duration: '5:54'},
  {label: 'Método e Classes', duration: '6:54'},
  {label: 'Leitura de arquivos', duration: '3:11'},
  {label: 'O que são Componentes e porque eles são importantes', duration: '2:10'},
  {label: 'Orientação a Objetos', duration: '1:15'},
  {label: 'Linguagem SQL', duration: '7:15'},
  {label: 'O que é Banco de Dados', duration: '10:21'},
  {label: 'O que são testes', duration: '3:15'},
  {label: 'O que é rest', duration: '6:48'},
  {label: 'O é Jsx', duration: '9:12'},
  {label: 'Como o react atualiza as views', duration: '7:43'},
]

function Block12() {
  const leftDropdowns = [1, 2, 3, 4, 9]
  const rightDropdowns = [5, 6, 7, 8, 10]
  
  const handleLeftDropdownsClick= (index) => {
    document.getElementById(`dropdown-items-${index}`).classList.toggle('is-hidden')
    leftDropdowns.filter(x=> x !== index).forEach(e => {
      document.getElementById(`dropdown-items-${e}`).classList.add('is-hidden')
    })
  }

  function handleRightDropdownsClick(index) {
    document.getElementById(`dropdown-items-${index}`).classList.toggle('is-hidden')
    rightDropdowns.filter(x => x !== index).forEach(e => {
      document.getElementById(`dropdown-items-${e}`).classList.add('is-hidden')
    })
  }

  return (
    <Bg>
      <div className="container">
        <div className="columns">
          <div className="column">
            <TitleDiv>
              <Title>
                <SizedTitle>M</SizedTitle>ódulos, <SizedTitle>A</SizedTitle>ulas, <SizedTitle>C</SizedTitle>onteúdos <SizedTitle>E</SizedTitle>xtras e
              </Title>
              <Title>
                <SizedTitle>G</SizedTitle>rupo de <SizedTitle>D</SizedTitle>iscussão do <SizedTitle>C</SizedTitle>urso
              </Title>
            </TitleDiv>
          </div>
        </div>

        <div className="columns">
          <div className="column is-6 is-offset-3">
            <YellowBlock>
              Programador em 8 semanas!
              <p className="is-size-5">Módulos e Aulas</p>
            </YellowBlock>
          </div>
        </div>

        <div className="columns">
          <div className="column is-6 is-multiline is-paddingless is-marginless">
            <Dropdown 
              number={1}
              label={'Introdução à Web'}
              items={items}
              handleOnClick={handleLeftDropdownsClick}
              />

            <Dropdown 
              number={2}
              label={'Passando Props entre os components Props entre'}
              items={items}
              handleOnClick={handleLeftDropdownsClick}
              />

            <Dropdown 
              number={3}
              label={'O que é Jsx'}
              items={items}
              handleOnClick={handleLeftDropdownsClick}
              />

            <Dropdown 
              number={4}
              label={'Resolução'}
              items={items}
              handleOnClick={handleLeftDropdownsClick}
              />
            </div>

            <div className="column is-6 is-multiline is-paddingless is-marginless">
              <Dropdown 
                number={5}
                label={'Components e props'}
                items={items}
                handleOnClick={handleRightDropdownsClick}
                />

              <Dropdown 
                number={6}
                label={'Exercicio'}
                items={items}
                handleOnClick={handleRightDropdownsClick}
                />

              <Dropdown 
                number={7}
                label={'Alterando o estado da classe'}
                items={items}
                handleOnClick={handleRightDropdownsClick}
                />

              <Dropdown 
                number={8}
                label={'Create React APP'}
                items={items}
                handleOnClick={handleRightDropdownsClick}
                />
            </div>
          </div>

        <div className="image">
          <BgExtras>
            <div className="columns">
              <div className="column is-12">
                <YellowBlock className="column is-5" style={{ maxWidth: '300px', padding: '5px' }}>
                  Conteúdos Extras
                </YellowBlock>
              </div>
            </div>

            <div className="columns column is-10 is-offset-1 is-paddingless">
              <div className="column is-6">
                <DropdownExtras 
                  number={9}
                  label={'Carreira'} 
                  items={items}
                  handleOnClick={handleLeftDropdownsClick}
                  />
              </div>

              <div className="column is-6">
                <DropdownExtras 
                  number={10}
                  label={'Mercado de Trabalho'}
                  items={items} 
                  handleOnClick={handleRightDropdownsClick}
                  />
              </div>
            </div>
          </BgExtras>
        </div>

      </div>
    </Bg>
  )
}

export default Block12;