import React from 'react'

import Block01 from '../../components/Block01'
import Block02 from '../../components/Block02'
import Block03 from '../../components/Block03'
import Block04 from '../../components/Block04'
import Block05 from '../../components/Block05'
import Block06 from '../../components/Block06'
import Block07 from '../../components/Block07'
import Block08 from '../../components/Block08'
import Block09 from '../../components/Block09'
import Block10 from '../../components/Block10'
import Block11 from '../../components/Block11'
import Block12 from '../../components/Block12'
import Block13 from '../../components/Block13'
import Block14 from '../../components/Block14'
import Block15 from '../../components/Block15'
import Block16 from '../../components/Block16'
import Block17 from '../../components/Block17'
import Block18 from '../../components/Block18'
import Block19 from '../../components/Block19'
import Block20 from '../../components/Block20'

function Home() {
  return (
    <div>
      <Block01 />
      <Block02 />
      <Block03 />
      <Block04 />
      <Block05 />
      
      <Block06 />
      <Block07 />
      <Block08 />
      <Block09 />
      <Block10 />
      <Block11 />
      <Block12 />
      <Block13 />
      <Block14 />
      <Block15 />
      
      <Block06 />
      <Block16 />
      <Block17 />
      <Block18 />
      
      <Block06 />
      <Block19 />
      <Block20 />
    </div>
  )
}

export default Home