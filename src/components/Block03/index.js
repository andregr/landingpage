import React from 'react'
import styled from 'styled-components'

import { TitleDiv, Title, SizedTitle, YellowTitle, SizedYellowTitle, Image, ParagraphBold } from '../StyledComponents'

import bgImage from '../../images/bg-03.png';
import imgFrontBackEnd from '../../images/img-front-back-end-03.png'
import imgJs from '../../images/img-js-03.png'
import imLogos from '../../images/img-logos-03.png'

const Bg = styled.div`
  background: #694ed8;
  background-image: url(${bgImage});
  padding-bottom: 50px;
  width: 100%;
`
function Block03() {
  return (
    <Bg>
      <div className="container">
        <div className="columns is-marginless">
          <TitleDiv className="column">
            <Title>
              <SizedTitle>M</SizedTitle>as, o que é 
              <SizedYellowTitle> ser </SizedYellowTitle> um 
              <SizedYellowTitle> p</SizedYellowTitle><YellowTitle>rogramador</YellowTitle>
              <SizedYellowTitle> f</SizedYellowTitle><YellowTitle>ull</YellowTitle>
              <SizedYellowTitle> s</SizedYellowTitle><YellowTitle>tack</YellowTitle>
              <SizedTitle>?</SizedTitle>
            </Title>
            
            <Title>
              <SizedTitle>E </SizedTitle> por quê 
              <SizedYellowTitle> j</SizedYellowTitle><YellowTitle>ava</YellowTitle>
              <SizedYellowTitle>s</SizedYellowTitle><YellowTitle>cript</YellowTitle>
              <SizedTitle>?</SizedTitle>
            </Title>
          </TitleDiv>
        </div>

        <div className="columns is-marginless">
          <div className="column is-5 is-offset-2">
            <ParagraphBold>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vehicula ipsum id lectus semper, 
              in auctor risus faucibus. Aliquam facilisis sapien ac ipsum ullamcorper ultricies. 
              Ut condimentum mauris lectus, in consequat lacus porta in. Nulla vestibulum dui sed justo viverra, 
              vel elementum ipsum vestibulum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, 
              per inceptos himenaeos. Nulla vitae neque nec est finibus hendrerit in sed odio. 
              Pellentesque quam eros, pharetra aliquam feugiat a, molestie eget turpis.
              Mauris feugiat varius sollicitudin. Integer aliquet, ante at fringilla dictum, 
              ipsum ligula facilisis turpis
            </ParagraphBold>
          </div>

          <div className="column is-3">
            <Image src={imgFrontBackEnd} alt="front backend" />
          </div>
        </div>

        <div className="columns" style={{ margin: '10px 0px 0px 0px'}}>
          <div className="column is-2 is-offset-2">
            <Image src={imgJs} alt="js logo"/>
          </div>

          <div className="column is-6">
            <ParagraphBold>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed vehicula ipsum id lectus semper, 
              in auctor risus faucibus. Aliquam facilisis sapien ac ipsum ullamcorper ultricies. 
              Ut condimentum mauris lectus, in consequat lacus porta in. Nulla vestibulum dui sed justo viverra, 
              vel elementum ipsum vestibulum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, 
              per inceptos himenaeos. Nulla vitae neque nec est finibus hendrerit in sed odio. 
            </ParagraphBold>

            <img src={imLogos} alt="logos"/>
          </div>
        </div>
      </div>
    </Bg>
  )
}

export default Block03;