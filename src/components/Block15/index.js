import React from 'react'
import styled from 'styled-components'

import { TitleDiv, Title, SizedTitle, YellowTitle, YellowSubtitle, WhiteText } from '../StyledComponents'

import bgImage from '../../images/bg-15.png'
import leo from '../../images/Leo-15.png'
import daniel from '../../images/Daniel-15.png'
import github from '../../images/github-15.png'
import linkedin from '../../images/linkedin-15.png'
import facebook from '../../images/facebook-15.png'

const Bg = styled.section`
  width: 100%;
  background-color: #251938;
  background-image: url(${bgImage});
  padding-bottom: 40px;
`

const LinkPaddedRight = styled.a`
  padding-right: 30px;
`

const LinkPaddedLeft = styled.a`
  padding-left: 30px;
`

const Image = styled.img`
  margin-top: 10px;
`

function Block15() {
  return (
    <Bg>
      <div className="container">
        <div className="columns">
          <div className="column">
            <TitleDiv>
              <Title>
                <SizedTitle>Q</SizedTitle>uem são os
                <YellowTitle><SizedTitle> P</SizedTitle>rofessores</YellowTitle> <SizedTitle>?</SizedTitle>
              </Title>
            </TitleDiv>
          </div>
        </div>

        <div className="columns is-vcentered">
          <div className="column is-4">
            <Image src={leo} alt="teacher Leonardo" />
          </div>

          <div className="column is-7">
            <YellowSubtitle>Leonardo Scorza</YellowSubtitle>
            <WhiteText>
              Programador Full Stack desde 2013, fundador e professor da startup de educação
              online para programadores OneBitCode (focada em Ruby on Rails)
              e Escola de JavaScript (focada em JavaScript).
            </WhiteText>
            <WhiteText>
              Sou entusiasta de todo o tipo de tecnologia, em especial tecnologias Open
              Source e quero facilitar a vida dos programadores através da criação de
              conteúdos educacionais de fácil e rápido entendimento.
            </WhiteText>

            <LinkPaddedRight href="#">
              <Image src={github} alt="Leonardo GitHub" />
            </LinkPaddedRight>

            <LinkPaddedRight href="#">
              <Image src={linkedin} alt="Leonardo LinkedIn" />
            </LinkPaddedRight>

            <LinkPaddedRight href="#">
              <Image src={facebook} alt="Leonardo Facebook" />
            </LinkPaddedRight>
          </div>
        </div>

        <div className="columns is-vcentered">
          <div className="column is-7 has-text-right">
            <YellowSubtitle>Daniel Moreto</YellowSubtitle>
            <WhiteText>
              Programador, Rubysta e praticante de Budo Taijutso. Adoro discutir ideias,
              estar inserido em ambientes inovativos e, é claro, programar. Acredito que
              a programação é um conhecimento que vai muito além de construir softwares.
            </WhiteText>

            <LinkPaddedLeft href="#">
              <Image src={github} alt="Daniel GitHub" />
            </LinkPaddedLeft>

            <LinkPaddedLeft href="#">
              <Image src={linkedin} alt="Daniel LinkedIn" />
            </LinkPaddedLeft>

            <LinkPaddedLeft href="#">
              <Image src={facebook} alt="Daniel Facebook" />
            </LinkPaddedLeft>
          </div>

          <div className="column is-4">
            <Image src={daniel} alt="teacher Daniel" />
          </div>
        </div>



      </div>
    </Bg>
  )
}

export default Block15