import React from 'react'
import styled from 'styled-components'

import { Image as CenteredImage } from '../StyledComponents'

import bgImage from '../../images/bg-13.png'

import relogio from '../../images/relogio-13.png'
import infinito from '../../images/infinito-13.png'
import logos from '../../images/logo-js8-13.png'
import calendario from '../../images/calendario-13.png'
import video from '../../images/video-13.png'

const Bg = styled.section`
  width: 100%;
  background-color: #251938;
  background-image: url(${bgImage});
`

const Image = styled.img`
  padding: 20px;
`

function Block13() {
  return(
    <Bg>
      <div className="container" style={{ paddingTop: '40px'}}>
        <div className="columns is-multiline">
          <div className="column is-3 is-offset-1 is-paddingless has-text-centered">
            <Image src={relogio} alt="clock"/>
          
            <Image src={infinito} alt="infinite"/>
          </div>

          <div className="column is-3 is-paddingless has-text-centered">
            <CenteredImage src={logos} alt="course logo"/>
          </div>

          <div className="column is-3 is-paddingless has-text-centered">
            <Image src={calendario} alt="calendar"/>

            <Image src={video} alt="video"/>
          </div>
        </div>
      </div>
    </Bg>
  )
}

export default Block13