import React from 'react'
import styled from 'styled-components'

import { YellowBlock, YellowTitle, Image, Paragraph20 as Paragraph } from '../StyledComponents'

import bgImage from '../../images/bg-14.png'
import discord from '../../images/discord-14.png'

const Bg = styled.section`
  width: 100%;
  background-color: #694ed8;
  background-image: url(${bgImage});
  padding-top: 80px;
`

const Text = styled.div`
  margin-top: 80px;

  @media screen and (max-width: 1407px) {
    margin-top: 70px;
  }

  @media screen and (max-width: 1215px) {
    margin-top: 30px;
  }

  @media screen and (max-width: 943px) {
    margin-top: 0px;
  }
`

function Block14() {
  return (
    <Bg>
      <div className="container">
        <div className="columns">
          <div className="column">
            <YellowBlock>
              Grupo de Discussão do Curso
            </YellowBlock>
          </div>
        </div>

        <div className="columns">
          <div className="column is-offset-1 is-5">
            <Image src={discord} alt="discord" />
          </div>

          <Text className="column is-6">
            <Paragraph>
              Faça parte da comunidade do Curso
              Programador Full Stack em 8 semanas
              e junte-se a dezenas de outras pessoas
              engajadas e com vontade de crescer no
              mundo da Programação Web 0/
            </Paragraph>

            <Paragraph>
              Compartilhe suas dúvidas e suas conquistas
              com a galera, faça um <YellowTitle className="is-uppercase">Networking</YellowTitle> poderoso
              e corra para o mercado de trabalho!
            </Paragraph>
          </Text>
        </div>
      </div>
    </Bg>
  )
}

export default Block14