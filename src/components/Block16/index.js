import React from 'react'
import styled from 'styled-components'

import { TitleDiv, Title, SizedTitle, YellowTitle, Image } from '../StyledComponents'

import bgImage from '../../images/bg-16.png'
import wiki from '../../images/wiki-js-16.png'
import noticia1 from '../../images/noticia1-16.png'
import noticia2 from '../../images/noticia2-16.png'
import noticia3 from '../../images/noticia3-16.png'
import separador from '../../images/separador-16.png'

const Bg = styled.section`
  width: 100%;
  background-color: #694ed8;
  background-image: url(${bgImage});
  padding-bottom: 40px;
`

function Block16() {
  return (
    <Bg>
      <div className="container">
        <div className="columns">
          <div className="column">
            <TitleDiv>
              <Title>
                <SizedTitle>P</SizedTitle>rogramação <SizedTitle>W</SizedTitle>eb: será que
              </Title>
              <Title style={{ marginTop: '5px'}}>
                <SizedTitle className="is-uppercase"><YellowTitle> Vale a pena</YellowTitle></SizedTitle> mesmo
                <SizedTitle> ?</SizedTitle>
              </Title>
            </TitleDiv>
          </div>
        </div>

        
        <div className="columns">
          <div className="column is-6">
            <Image src={wiki} alt="wikipedia"/>
          </div>

          <Image className="is-hidden-mobile" src={separador} alt="separator" />

          <div className="column is-6">
            <div className="columns">
              <div className="column image">
                <a href="https://tiinside.com.br/tiinside/18/06/2019/falta-de-profissionais-de-ti-prejudica-crescimento-do-setor-aponta-assespro-pr/" target="_blank" rel="noopener noreferrer">
                  <Image src={noticia1} alt="news 1"/>
                </a>
              </div>
              <div className="column image">
                <a href="https://www.jornalcontabil.com.br/saiba-quais-sao-os-profissionais-da-area-de-tecnologia-mais-procurados-em-2019/" target="_blank" rel="noopener noreferrer">
                  <Image src={noticia2} alt="news 2"/>
                </a>
              </div>
            </div>

            <div className="columns">
              <div className="column">
                <a href="https://veja.abril.com.br/economia/mercado-de-trabalho-as-profissoes-que-mais-devem-contratar-em-2019/" target="_blank" rel="noopener noreferrer">
                  <Image src={noticia3} alt="news 3"/>
                </a>
              </div>
            </div>
          </div>
        </div>

      </div>
    </Bg>
  )
}

export default Block16