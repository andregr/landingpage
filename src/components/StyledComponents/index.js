import styled from 'styled-components'

export const TitleDiv = styled.div`
  padding: 40px 0px 0px 0px;
  margin: 0px 0px 30px 0px;
`

export const Title = styled.h1`
  color: white;
  text-shadow: 2px 2px black;
  text-transform: uppercase;
  padding: 0px 10px;
  margin: 0px;
  font-size: 25px;
  line-height: 30px;
  font-weight: 600;
  text-align: center;
  letter-spacing: .02em;

  @media screen and (max-width: 330px) {
    font-size: 20px;
    line-height: 30px;
  }
`

export const SizedTitle = styled.span`
  font-size: 32px;
  font-weight: 700;

  @media screen and (max-width: 330px) {
    font-size: 25px;
  }
`

export const PurpleTitle = styled.h1`
  color: #251938;
  text-shadow: 3px 3px #fff;
  text-transform: uppercase;
  padding: 0px 10px;
  margin: 0px;
  font-size: 25px;
  line-height: 30px;
  font-weight: 700;
  text-align: center;

  @media screen and (max-width: 330px) {
    font-size: 20px;
    line-height: 30px;
  }
`

export const YellowTitle = styled.span`
  color: #fecb2f;

  @media screen and (max-width: 330px) {
    font-size: 20px;
  }
`

export const SizedYellowTitle = styled.span`
  color: #fecb2f;
  font-size: 32px;
  font-weight: 700;

  @media screen and (max-width: 330px) {
    font-size: 25px;
  }
`

export const Subtitle = styled.h2`
  color: white;
  text-align: center;
  text-transform: uppercase;
  padding: 0px;
  margin: 0px;
  font-weight: bold;
  font-size: 20px;
  line-height: 25px;
`

export const YellowSubtitle = styled.h2`
  color: #fecb2f;
  font-size: 20px;
  font-weight: 600;
`

export const SizedSubtitle = styled.span`
  font-size: 30px;
`

export const Paragraph = styled.p`
color: white;
font-size: 14px;
margin-top: 10px;
`

export const Paragraph20 = styled.p`
  font-size: 20px;
  font-weight: 600;
  color: #FFF;
  line-height: 1.4em;
  border-left: 3px solid #00f7ff;
  padding: 5px 20px;

  @media screen and (max-width: 568px) {
    font-size: 18px;
}
`

export const ParagraphBold = styled.p`
  color: white;
  font-weight: bold;
  text-align: justify;
  padding: 5px;
`

export const Image = styled.img`
  display: block;
  margin: 0 auto;
`

export const Text = styled.div`
  color: white;
  font-size: 14px;
  padding: 0px 5px;
  margin-top: 10px;
`

export const WhiteText = styled.p`
  font-size: 16px;
  color: #fff;
`

export const SpanBold = styled.span`
  color: white;
  font-size: 14px;
  font-weight: bold;
`

export const FloatingImgClickHere = styled.div`
  position: relative;
  left: -95px;
  top: 45px;
  max-height: 130px;
  max-width: 150px;

  @media screen and (max-width: 768px) {
    max-height: 100px;
    max-width: 130px;
    left: -5px;
    top: 40px;
  }

  @media screen and (max-width: 580px) {
    max-height: 50px;
    max-width: 80px;
    left: -10px;
    top: 20px;
  }
`

export const FloatingImgSubscribe = styled.div`
  position: relative;

  @media screen and (max-width: 768px) {
    width: 75%;
    margin: 0 auto;
  }

  @media screen and (max-width: 580px) {
    width: 75%;
    margin: 0 auto;
  }
`

export const YellowBlock = styled.div`
  background-color: #fecb2f;
  color: white;
  font-size: 26px;
  font-weight: 600;
  text-shadow: 2px 2px 2px #555;
  text-align: center;
  margin: 0 auto;
  max-width: 600px;
  box-shadow: 7px 7px 0px rgba(0, 0, 0, 0.4);
`