import React from 'react'
import styled from 'styled-components'

import { TitleDiv, Title, SizedTitle, YellowTitle, Image, YellowBlock, Paragraph20 as Paragraph } from '../StyledComponents'

import bgImage from '../../images/bg-15.png'
import gadgets from '../../images/img-17.png'
import separator from '../../images/separador-17.png'
import points from '../../images/bolinhas-17.png'

const Bg = styled.section`
  width: 100%;
  background-color: #251938;
  background-image: url(${bgImage});
  padding-bottom: 40px;
`

const Subtitle = styled.h2`
  font-size: 24px;
  font-weight: 600;
  color: #FFF;
  margin-top: 10px;
`

function Block17() {
  return(
    <Bg>
      <div className="container">
        <div className="columns">
          <div className="column">
            <TitleDiv>
              <Title>
                <SizedTitle>M</SizedTitle>as precisa 
                <YellowTitle>
                  <SizedTitle> Estar preparador (A)</SizedTitle>
                </YellowTitle>
                <SizedTitle>!</SizedTitle>
              </Title>
            </TitleDiv>
          </div>
        </div>

        <div className="columns is-vcentered">
          <div className="column is-4">
            <Image src={gadgets} alt="gadgets"/>
          </div>

          <div className="column is-8">
            <YellowBlock style={{ margin: '0', padding: '10px 0' }}>
              Programador Full Stack JavaScript
            </YellowBlock>
            
            <Subtitle>
              Plano de Estudo:
            </Subtitle>
            
            <Paragraph style={{ fontWeight: 'normal', border: 'none', padding: '0' }}>
              para que em até 8 semanas você consiga buscar 
              seu primeiro Job como Full Stack JavaScript Júnior.
            </Paragraph>

            <Subtitle>
              Conteúdos Extras:
            </Subtitle>
            
            <Paragraph style={{ fontWeight: 'normal', border: 'none', padding: '0' }}>
              entrevistas, carreira, mercadod e trabalho, portfólio, home-office.
            </Paragraph>

            <Subtitle>
              Networking:
            </Subtitle>
            
            <Paragraph style={{ fontWeight: 'normal', border: 'none', padding: '0' }}>
              acesso ilimitado ao grupo de alunos 24 horas por dia.
            </Paragraph>

            <Subtitle>
              Portfólio:
            </Subtitle>
            
            <Paragraph style={{ fontWeight: 'normal', border: 'none', padding: '0' }}>
              ativo e com um projeto real desenvolvido para que
              você obtenha experiência antes mesmo de aplicar
              para uma vaga.
            </Paragraph>
          </div>

        </div>
      
        <div className="column" style={{ marginTop: '30px'}}>
          <div className="column">
            <Title>
              <span className="is-lowercase">e você</span> <YellowTitle>Vai </YellowTitle>
              estar preparado <SizedTitle>(</SizedTitle>a<SizedTitle>)</SizedTitle>!
            </Title>
          </div>
        </div>

        <div className="columns has-text-centered">
          <div className="column is-8 is-offset-2">
            <Paragraph style={{ border: 'none', fontWeight: 'normal' }}>
              Todos os <span className="has-text-weight-bold">conteúdos técnicos</span> em um plano de estudos de 8 semanas,
              módulos extras sobre <span className="has-text-weight-bold">carreira</span> e <span className="has-text-weight-bold">mercado de trabalho</span>, entrevistas exclusivas
              com <span className="has-text-weight-bold">programadores experientes</span>, grupo de discussão dos alunos <span className="has-text-weight-bold">24 horas</span> por dia,
              <span className="has-text-wieght-bold"> prática</span> desde a primeira aula até o projeto final e desenvolvimento de um 
              <span className="has-text-weight-bold"> APP real</span> em seu portfólio.
            </Paragraph>
          </div>
        </div>

        <div className="columns">
          <Image src={separator} alt="separator"/>
        </div>

        <div className="columns has-text-centered">
          <div className="column">
            <Title>
              <YellowTitle>
                Acesso Vitalício!
              </YellowTitle>
            </Title>

            <Image src={points} alt="points" style={{ margin: '15px auto'}}/>

            <Subtitle>
              TUDO <span className="has-text-weight-normal">em até</span> 12X <span className="has-text-weight-normal">de</span> R$42,00
            </Subtitle>
            
            <span className="is-block" style={{ color: '#FFF', fontSize: '16px' }}>ou</span>

            <Paragraph style={{ border: 'none'}} >
              R$490,00 <span className="has-text-weight-normal">à vista</span>
            </Paragraph>
          </div>
        </div>

      </div>
    </Bg>
  )
}

export default Block17;