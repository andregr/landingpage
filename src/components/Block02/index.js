import React from 'react'
import styled from 'styled-components'

import logos from '../../images/logos-02.png'

const Image = styled.img`
  margin: 50px auto;
  padding: 10px;

  @media screen and (max-width: 580px) {
    margin: 10px auto;
  }
`;

function Block02() {
  return(
    <div className="container">
      <div className="columns is-marginless is-paddingless">
        <Image src={logos} alt="logos"/>
      </div>
    </div>
  )
}

export default Block02;