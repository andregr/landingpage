import React from 'react'
import styled from 'styled-components'

import { TitleDiv, SizedTitle, PurpleTitle, Image } from '../StyledComponents'

import pcImage from '../../images/img-11.png'
import textImage from '../../images/img-txt-11.png'

const Bg = styled.div`
  width: 100%;
  background-color: #fecb2f;
`

const Paragraph = styled.p`
  font-size: 18px;
  text-align: center;
  color: #251938;
  line-height: 30px;
  max-width: 950px;
  margin: 0 auto;
  margin-bottom: 10px;
`

const PurpleText = styled.p`
  color: #694ed8;
  font-size: 30px;
  line-height: 40px;  
  text-transform: uppercase;
  font-weight: 700;
  text-align: right;
  padding-left: 30px;

  @media screen and (max-width: 1407px) {
    font-size: 25px;
    line-height: 30px; 
  }

  @media screen and (max-width: 1215px) {
    font-size: 18px;
    line-height: 25px; 
  }

  @media screen and (max-width: 768px){
    text-align: center;
    padding-left: 0px;
    font-size: 28px;
    line-height: 35px;
  } 
`
const RedText = styled.span`
  color: #f85d59;
  font-size: 30px;
  line-height: 40px;
  font-weight: 600;
  text-transform: uppercase;
  font-weight: 700;

  @media screen and (max-width: 1407px) {
    font-size: 25px;
    line-height: 30px; 
  }

  @media screen and (max-width: 1215px) {
    font-size: 18px;
    line-height: 25px; 
  }

  @media screen and (max-width: 768px) {
    font-size: 28px;
    line-height: 35px;
  } 
`

const SizedPurpleText = styled.span`
  font-size: 34px;
  font-weight: 700;

  @media screen and (max-width: 1407px) {
    font-size: 30px;
    line-height: 35px; 
  }

  @media screen and (max-width: 1215px) {
    font-size: 25px;
    line-height: 28px; 
  }

  @media screen and (max-width: 768px) {
    font-size: 32px;
  }
`

const SizedPurpleText2 = styled.span`
  font-size: 34px;
  font-weight: 700;


  @media screen and (max-width: 1407px) {
    font-size: 30px;
    line-height: 35px; 
  }
  
  @media screen and (max-width: 1215px) {
    font-size: 25px;
    line-height: 28px; 
  }

  @media screen and (max-width: 768px) {
    font-size: 32px;
  }

  &:before {
    content: "";

    @media screen and (min-width: 768px) {
      display: block;
    }
  }
`

function Block11() {
  return (
    <Bg>
      <div className="container">
        <div className="columns is-marginless">
          <div className="column">
            <TitleDiv style={{ marginBottom: '10px'}}>
              <PurpleTitle>
                <SizedTitle>M</SizedTitle>uito mais do que <SizedTitle>A</SizedTitle>ulas <SizedTitle>C</SizedTitle>omuns, aqui você
              </PurpleTitle>
            </TitleDiv>

            <TitleDiv style={{ paddingTop: '0px'}}>
              <PurpleTitle>
                <SizedTitle>Mergulha</SizedTitle> no universo da <SizedTitle>P</SizedTitle>rogramação <SizedTitle>F</SizedTitle>ull <SizedTitle>S</SizedTitle>tack
              </PurpleTitle>
            </TitleDiv>
          </div>
        </div>

        <div className="columns is-marginless">
          {/* <div className="column is-9">
            <Image src={textImage} alt="text"/>
          </div> */}

          <div className="column is-2">
            <PurpleText>
              Isso parece <SizedPurpleText><RedText>grego</RedText></SizedPurpleText> para <SizedPurpleText>você</SizedPurpleText><SizedPurpleText2>?</SizedPurpleText2>
            </PurpleText>
          </div>

          <div className="column is-6">
          <Image src={textImage} alt="text"/>
          </div>

          <div className="column is-3">
            <Image src={pcImage} alt="pc"/>
          </div>
        </div>

        <div className="columns is-marginless">
          <div className="column">
            <Paragraph>
              Então você tem <span className="has-text-weight-bold">mais um motivo</span> para <span className="has-text-weight-bold">fazer parte desse curso</span> e ir <span className="has-text-weight-bold">ir além</span> das aulas teóricas columns,
              pois aqui nós te <span className="has-text-weight-bold">mostramos a prática</span> e unimos o conhecimento transmitido ao <span className="has-text-weight-bold">mundo real, </span>
              apresentando entrevistas com programadores experientes, proporcionando um ambiente de troca de
              informações e aprendizados e te damos uma perspectiva do mercado de trabalho atual.
            </Paragraph>

            <Paragraph className="has-text-weight-bold">
              Sem enrolação, fácil e direto ao seu objetivo.
            </Paragraph>

            <Paragraph style={{ marginBottom: '0px'}}>
              Entenda de uma vez por todas o universo da Programação Full Stack e
            </Paragraph>

            <Paragraph className="has-text-weight-bold" style={{ marginBottom: '30px'}}>
              agarre as melhores oportunidades!
            </Paragraph>
          </div>
        </div>
      </div>
    </Bg>
  )
}

export default Block11