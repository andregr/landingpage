import React from 'react'
import styled from 'styled-components'

import { TitleDiv, PurpleTitle } from '../StyledComponents'

const Bg = styled.section`
  width: 100%;
  background-color: #fecb2f;
  padding-bottom: 40px;
`

const PurpleBlock = styled.div`
  background-color: #694ed8;
  color: white;
  font-size: 20px;
  font-weight: 700;
  display: block;
  padding: 10px;
  max-width: 320px
  margin: 5px;
  text-align: center;

  margin-left: ${props => props.right ? 'auto' : '5px'};
  margin-right: ${props => props.right ? '5px' : 'auto'};
`

const WhiteBlock = styled.div`
  padding: 20px 30px;
  margin: 5px;
  background-color: #FFF;
  color: #251938;
  font-size: 16px;
  box-shadow: 4px 4px rgba(0, 0, 0, 0.3);
`

function Block19() {
  return (
    <Bg>
      <div className="container">
        <div className="columns">
          <div className="column">
            <TitleDiv>
              <PurpleTitle style={{ fontSize: '40px'}}>
                FAQ
              </PurpleTitle>
            </TitleDiv>
          </div>
        </div>

        <div className="columns">
          <div className="column">
            <PurpleBlock right={false}>
              Essa é uma pergunta?
            </PurpleBlock>

            <WhiteBlock>
              Essa é a resposta desa pergunta. 
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent viverra justo sed tellus ullamcorper 
              efficitur. Ut lacinia hendrerit volutpat. Quisque pulvinar mauris diam, ac accumsan sem tincidunt ac. 
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc egestas, sapien quis dapibus commodo, 
              neque purus aliquam tortor, id tristique risus nisi non ante. Vestibulum eu velit a dui imperdiet 
              fermentum et eget neque. Mauris vel accumsan turpis, non cursus purus. Nullam maximus eu justo eget 
              vehicula. Cras ut placerat tellus. Etiam eget luctus eros. Vivamus sed consequat eros.
            </WhiteBlock>
          </div>
        </div>

        <div className="columns">
          <div className="column">
            <PurpleBlock right={true}>
              Essa é uma pergunta?
            </PurpleBlock>

            <WhiteBlock>
              Essa é a resposta desa pergunta. 
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent viverra justo sed tellus ullamcorper 
              efficitur. Ut lacinia hendrerit volutpat. Quisque pulvinar mauris diam, ac accumsan sem tincidunt ac. 
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc egestas, sapien quis dapibus commodo, 
              neque purus aliquam tortor, id tristique risus nisi non ante. Vestibulum eu velit a dui imperdiet 
              fermentum et eget neque. Mauris vel accumsan turpis, non cursus purus. Nullam maximus eu justo eget 
              vehicula. Cras ut placerat tellus. Etiam eget luctus eros. Vivamus sed consequat eros.
            </WhiteBlock>
          </div>
        </div>

      </div>
    </Bg>
  )
}

export default Block19