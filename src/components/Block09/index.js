import React from 'react'
import styled from 'styled-components'

import { TitleDiv, SizedTitle, PurpleTitle, Image } from '../StyledComponents'

import pcImage from '../../images/pc-9.png'

const Bg = styled.div`
  width: 100%;
  background-color: #fecb2f;
`
const PurpleText = styled.p`
  font-size: 16px;
  color: #251938;
`

function Block09() {
  return (
    <Bg>
      <div className="container">
        <div className="columns is-marginless">
          <div className="column">
            <TitleDiv>
              <PurpleTitle>
                <SizedTitle>M</SizedTitle>as e sobre as <SizedTitle>E</SizedTitle>ntrevitas
                <SizedTitle>?</SizedTitle>
              </PurpleTitle>
            </TitleDiv>
          </div>
        </div>

        <div className="columns is-marginless">
          <div className="column is-6">
            <Image src={pcImage} alt="pc"/>
          </div>

          <div className="column is-6">
            <PurpleText>
              A gente sabe que uma das maiores dificuldades para quem está
              começando é encontrar <span className="has-text-weight-bold">boas referências, dicas </span> do que fazer e do
              que não fazer, como <span className="has-text-weight-bold">melhorar e acelerar</span> se processo de aprendizado
              entre muitas outras coisas.
            </PurpleText>

            <PurpleText>
              Então, vamos trazer diferentes programadores para bater um papo
              e responder às dúvidas mais frequentes.
            </PurpleText>

            <PurpleText style={{ marginTop: '15px'}}>
              Eles vão contar para nós <span className="has-text-weight-bold">suas experiências, </span> como conseguiram
              <span className="has-text-weight-bold"> superar desafios </span> e vão dar dicas de como <span className="has-text-weight-bold">evoluir no seu job, </span>como se
              tornar um <span className="has-text-weight-bold">freelancer, </span> como trabalhar de qualquer lugar do mundo
              (remoto) e como <span className="has-text-weight-bold">gerenciar a carreira</span> e continuar evoluindo sempre.
            </PurpleText>

            <PurpleText style={{ marginTop: '15px', marginBottom: '30px'}}>
              Essa é uma sessão extra do curso Programador Full Stack JavaScript
              em 8 semanas, um <span className="has-text-weight-bold">conteúdo que fará a diferença</span> em sua visão como programador.
            </PurpleText>
          </div>
        </div>
      </div>
    </Bg>
  )
}

export default Block09