import React from 'react'
import styled from 'styled-components'

import { Subtitle, SizedSubtitle, Paragraph, FloatingImgClickHere, FloatingImgSubscribe } from '../StyledComponents';

import bgImage from '../../images/bg-01.png'
import separator from '../../images/separador-01.png'
import button from '../../images/botao-inscrever-01.png';
import clickHere from '../../images/clique-aqui-01.png';
import downArrow from '../../images/seta-01.png';

const Bg = styled.div`
  width: 100%;
  background-color: #251938;
  background-image: url(${bgImage});
`
const Title = styled.h1`
  color: white;
  text-shadow: 5px 5px black;
  text-align: center;
  text-transform: uppercase;
  padding: 0px;
  margin: 0px;
  font-size: 35px;
  line-height: 45px;
  font-weight: bold;

  @media screen and (max-width: 330px) {
    font-size: 30px;
    line-height: 40px;
  }
`

const BlueTitle = styled.span`
  color: #00f7ff;
  font-size: 40px;
  @media screen and (max-width: 330px) {
    font-size: 35px;
  }
`

const YellowTitle = styled.span`
  color: #fecb2f;
  font-size: 40px;
  @media screen and (max-width: 330px) {
    font-size: 35px;
  }
`

const RedTitle = styled.span`
  color: #f85d59;
`

const RedSizedTitle = styled.span`
  color: #f85d59;
  font-size: 55px;
  @media screen and (max-width: 330px) {
    font-size: 50px;
  }
`
const SeeMore = styled.h2`
  color: white;
  text-align: center;
  text-transform: uppercase;
  font-weight: bold;
  font-size: 18px;
`
const DownArrow = styled.img`
  margin: 0 auto;
  height: 35px !important;
  width: 70px !important;

  @media screen and (max-width: 768px) {
    height: 25px !important;
    width: 50px !important;
  }
`

function Block01() {
  return (    
    <Bg>
      <div className="container">
          <div className="columns is-marginless" style={{ paddingTop: '50px' }}>
            <div className="column">
              <Title>
                Do <BlueTitle>Zero</BlueTitle> a <BlueTitle>Programador</BlueTitle>
              </Title>
              <Title>
                Full Stack <YellowTitle>Javascript</YellowTitle> em <RedSizedTitle>8</RedSizedTitle> <RedTitle>Semanas</RedTitle><RedSizedTitle>!</RedSizedTitle>
              </Title>
            </div>
          </div>
          
          <div className="columns is-marginless">
            <div className="column is-12">
              <img src={separator} alt="separator" />
            </div>
          </div>

          <div className="columns is-marginless" style={{ paddingTop: '20px' }}>
            <div className="column is-6 is-offset-1" style={{ backgroundColor: '#000' }}>

            </div>

            <div className="column is-4" style={{ marginLeft: '10px'}}>
              <Subtitle>
                Conteúdo <SizedSubtitle>100%</SizedSubtitle> focado no mercado de trabalho
              </Subtitle>

              <Paragraph>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi ipsum metus, bibendum sed viverra ut, sagittis in nisi. In hac habitasse platea dictumst.
              </Paragraph>

              <Paragraph>
                Sed blandit ac magna ut iaculis. Aliquam facilisis ex ultricies, volutpat mi eu, euismod augue. Nulla mattis odio turpis, quis auctor nunc rhoncus faucibus.
              </Paragraph>

              <Paragraph>
                liquam erat volutpat. Morbi blandit leo sit amet augue interdum dapibus. Proin vel commodo ipsum. Quisque quam risus, rhoncus posuere interdum sed, tincidunt a ante. Quisque ultricies est quis neque porta, id sodales turpis varius.
              </Paragraph>
            </div>
          </div>

          <div className="columns is-marginless">
            <div className="column is-6 is-offset-3">
              <FloatingImgClickHere className="image">
                <img src={clickHere} alt="click here" />
              </FloatingImgClickHere>
              <FloatingImgSubscribe className="image">
                <a href="#">
                  <img src={button} alt="subscribe button" />
                </a>
              </FloatingImgSubscribe>
            </div>
          </div>

          <div className="columns is-marginless">
            <div className="column is-12">
              <SeeMore>
                Veja todo o conteúdo abaixo
              </SeeMore>
            </div>
          </div>

          <div className="columns is-marginless">
            <div className="column is-12 image" >
              <DownArrow src={downArrow} alt="down arrow" />
            </div>
          </div>
        </div>
    </Bg>
  )
}

export default Block01